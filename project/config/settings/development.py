# Python imports
import os

# fetch the common settings
from .common import *
from .extra import *

# ##### APPLICATION CONFIGURATION #########################

# allow all hosts during development
ALLOWED_HOSTS = ['*']


INSTALLED_APPS = DEFAULT_APPS
INSTALLED_APPS += [
    'rest_framework',
    #apps
    'users',
    'tasks',
    'notifications',
]


# ##### DATABASE CONFIGURATION ############################
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': str(PROJECT_ROOT / 'run' / 'dev.sqlite3'),
#     }
# }
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'database_name',
        'USER': 'database_user',
        'PASSWORD': 'database_password',
        'HOST': 'db',
        'PORT': '5432',
    }
}


# ##### DEBUG CONFIGURATION ###############################
DEBUG = True


####### CORS SETTINGS ################

CORS_ORIGIN_ALLOW_ALL = True
