from django.contrib import admin
from django.urls import path, include


apipatterns = [
    path('tasks/', include('tasks.urls')),
    path('users/', include('users.urls'))
]

urlpatterns = [
    path('api/', include(apipatterns)),
    path('admin/', admin.site.urls),
]


