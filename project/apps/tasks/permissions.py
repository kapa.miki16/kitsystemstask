from rest_framework.permissions import BasePermission


class IsTaskOwner(BasePermission):
    message = 'You must be the owner'

    def has_object_permission(self, request, view, obj):
        user = request.user
        return obj.author == user