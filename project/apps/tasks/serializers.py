from rest_framework import serializers

from .models import Task, TaskParticipant
from users.models import CustomUser


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = (
            'name',
            'description',
            'deadline',
            'status',
        )


class TaskCreateSerializer(serializers.ModelSerializer):
    users = serializers.PrimaryKeyRelatedField(
        queryset=CustomUser.objects.all(),
        allow_null=True,
        allow_empty=True,
        many=True,
    )

    class Meta:
        model = Task
        fields = (
            'name',
            'description',
            'deadline',
            'users'
        )

    def create(self, validated_data):
        author = self.context['request'].user
        data = validated_data.copy()
        participants = data.pop('users', None)
        task = Task.objects.create(
            author=author,
            **data
        )

        if participants:
            for user in participants:
                TaskParticipant.objects.create(
                    task=task,
                    user=user
                )

        return validated_data


class TaskParticipantUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = TaskParticipant
        fields = (
            'done',
        )