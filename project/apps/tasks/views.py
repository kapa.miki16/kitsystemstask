from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .permissions import IsTaskOwner
from utils.functions import get_object_or_404
from .models import Task, TaskParticipant
from .serializers import (
    TaskSerializer,
    TaskCreateSerializer,
    TaskParticipantUpdateSerializer
)


class TaskViewSet(ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        serializer_class = self.serializer_class
        if self.action == 'create':
            serializer_class = TaskCreateSerializer
        elif self.action == 'done':
            serializer_class = TaskParticipantUpdateSerializer
        return serializer_class

    def get_permissions(self):
        if self.action == 'update':
            permission_classes = [IsAuthenticated, IsTaskOwner]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]

    def create(self, request, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)

    def update(self, request, uuid, *args, **kwargs):
        task = get_object_or_404(Task, uuid=uuid)
        self.check_object_permissions(request, obj=task)
        serializer = self.serializer_class(task, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    def done(self, request, uuid, *args, **kwargs):
        task = get_object_or_404(Task, uuid=uuid)

        try:
            task_participant = TaskParticipant.objects.get(task=task, user=request.user)
            print(task_participant)
            if task_participant.done:
                data = {'detail': 'Task already is done'}
                status = 400
            else:
                print('first line in else')
                serializer_class = self.get_serializer_class()
                serializer = serializer_class(task_participant, data=request.data)
                print('after serializer')
                serializer.is_valid(raise_exception=True)
                print('after is valid')
                serializer.save()
                print('after save')
                status = 200
                data = serializer.data
                print('last code in else')
        except Exception as error:
            print(str(error))
            status = 403
            data = {"detail": "You don't have access"}

        return Response(status=status, data=data)


