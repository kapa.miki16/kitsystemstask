from django.db import models

from config.settings.common import AUTH_USER_MODEL
from utils.models import DateModel


class Task(DateModel):
    STATUS_CHOICES = (
        (1, 'Active'),
        (2, 'Review'),
        (3, 'Completed')
    )
    author = models.ForeignKey(
        AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    name = models.CharField(
        max_length=256,
    )
    description = models.TextField(
        blank=True,
        null=True
    )
    deadline = models.DateTimeField(
        blank=True,
        null=True
    )
    status = models.PositiveSmallIntegerField(
        choices=STATUS_CHOICES,
        default=1
    )

    class Meta:
        ordering = ('-created_at',)

    def __str__(self):
        return self.name


class TaskParticipant(DateModel):
    task = models.ForeignKey(
        Task,
        on_delete=models.CASCADE,
        related_name='participants'
    )
    user = models.ForeignKey(
        AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    start_time = models.DateTimeField(
        blank=True,
        null=True
    )
    completion_time = models.DateTimeField(
        blank=True,
        null=True
    )
    done = models.BooleanField(
        default=False
    )

    def __str__(self):
        return f'Participant: {self.user.email}. Task: {self.task.name}'
