from django.urls import path

from .views import TaskViewSet


urlpatterns = [
    path('create/', TaskViewSet.as_view({'post': 'create'})),
    path('update/<str:uuid>/', TaskViewSet.as_view({'put': 'update'})),
    path('done/<str:uuid>/', TaskViewSet.as_view({'put': 'done'}))
]


