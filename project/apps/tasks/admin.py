from django.contrib import admin

from .models import Task, TaskParticipant


admin.site.register(TaskParticipant)


class TaskParticipantInline(admin.StackedInline):
    model = TaskParticipant
    extra = 0
    classes = ['collapse']


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    inlines = [TaskParticipantInline]
    readonly_fields = ('uuid', 'created_at', 'updated_at',)
    list_display = (
        'name',
        'author',
        'deadline',
        'status'
    )
    fieldsets = (
        ('Main', {
            'fields': ('author', 'name', 'description', 'deadline', 'status')
        }),
        ('Additional Information', {
            'classes': ('collapse',),
            'fields': ('uuid', 'created_at', 'updated_at',)
        })
    )
