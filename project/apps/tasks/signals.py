from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from notifications.models import Notification
from .models import Task, TaskParticipant


@receiver(post_save, sender=Task)
def on_update(sender, instance, created, **kwargs):
    if not created:        # when task updated
        for participant in instance.participants.all():
            notification = Notification.objects.create(
                task=instance,
                _type=2,
                received_user=participant.user
            )


@receiver(pre_save, sender=TaskParticipant)
def on_done_task(sender, instance, **kwargs):
    try:
        previous = TaskParticipant.objects.get(uuid=instance.uuid)
    except:
        previous = None

    if previous:  # If object already exists
        previous = TaskParticipant.objects.get(uuid=instance.uuid)  # Get previous data
        if previous.done != instance.done:  # when the task is completed
            notification = Notification.objects.create(
                task=instance.task,
                _type=3,
                received_user=instance.task.author,
                completed_task_user=instance.user
            )


@receiver(post_save, sender=TaskParticipant)
def on_create_task(sender, instance, created, **kwargs):
    if created:        # when task updated
        received_user = instance.user
        notification = Notification.objects.create(
            task=instance.task,
            _type=1,
            received_user=received_user
        )

