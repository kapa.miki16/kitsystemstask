from django.db import models

from tasks.models import Task
from utils.models import DateModel
from config.settings.common import AUTH_USER_MODEL


class Notification(DateModel):
    TYPE_CHOICES = (
        (1, 'Task created'),
        (2, 'Task updated'),
        (3, 'Task is done')
    )
    task = models.ForeignKey(
        Task,
        on_delete=models.CASCADE
    )
    received_user = models.ForeignKey(
        AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    sent = models.BooleanField(
        default=False
    )
    _type = models.PositiveSmallIntegerField(
        choices=TYPE_CHOICES,
        blank=True,
        null=True
    )
    completed_task_user = models.ForeignKey(  # will be filled in when the task is completed. It's not the best solution
        AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name='notifications'
    )
    text = models.TextField()

    def __str__(self):
        return self.task.name
