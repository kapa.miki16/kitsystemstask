from django.core.mail import send_mail
import time

from config.celery import app
from notifications.models import Notification
from config.settings.extra import EMAIL_HOST_USER


@app.task
def async_send_mail(notification_uuid):
    # This is so that the data can be updated.
    # An exception will be thrown without this part of the code.
    time.sleep(2.5)

    instance = Notification.objects.get(uuid=notification_uuid)

    if instance._type == 1:
        text = f'New task from {instance.task.author}'
    elif instance._type == 2:
        text = f'{instance.task.name} task was updated'
    else:
        text = f'{instance.completed_task_user} completed the task.'

    send_mail('KitSystems', text, EMAIL_HOST_USER, [instance.received_user])
    instance.text = text
    instance.sent = True
    instance.save(update_fields=['text', 'sent'])
