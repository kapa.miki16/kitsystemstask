from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Notification
from .tasks import async_send_mail


@receiver(post_save, sender=Notification)
def create_notification(sender, instance, created, **kwargs):
    if created:
        async_send_mail.delay(str(instance.uuid))
