from uuid import uuid4
from django.db import models


class BaseModel(models.Model):
    uuid = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid4,
    )

    class Meta:
        abstract = True
        ordering = ('uuid', )


class DateModel(BaseModel):
    created_at = models.DateTimeField(
        auto_now_add=True,
    )
    updated_at = models.DateTimeField(
        auto_now=True,
    )

    class Meta:
        abstract = True
        ordering = ('created_at', 'updated_at')
