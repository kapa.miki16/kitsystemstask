from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import CustomUser


@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    readonly_fields = ('uuid', 'created_at', 'updated_at', 'last_login',)
    ordering = ('created_at',)
    list_display = (
        'email',
        'phone',
        'is_staff',
        'is_active',
        'created_at',
        'updated_at',
    )
    list_filter = (
        'is_staff',
        'is_active',
    )
    search_fields = (
        'email',
    )
    fieldsets = (
        ('Main', {
            'fields': ('email', 'phone', 'password',)
        }),
        ('Person', {
            'fields': ('last_name', 'first_name', 'gender', 'avatar',)
        }),
        ('Permissions', {
            'classes': ('collapse',),
            'fields': ('is_superuser', 'is_staff', 'is_active', 'groups', 'user_permissions',)
        }),
        ('Additional Information', {
            'classes': ('collapse',),
            'fields': ('uuid', 'created_at', 'updated_at', 'last_login',)
        })
    )
    add_fieldsets = (
        ('Main', {
            'classes': ('wide',),
            'fields': ('email', 'phone', 'password1', 'password2', 'is_staff', 'is_active',)
        }),
        ('Person', {
            'classes': ('collapse',),
            'fields': ('last_name', 'first_name', 'gender', 'avatar',)
        }),
    )
