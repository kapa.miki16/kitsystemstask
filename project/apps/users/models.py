from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin

from utils.models import DateModel
from .managers import CustomUserManager


class CustomUser(DateModel, AbstractBaseUser, PermissionsMixin):
    GENDER_CHOICES = (
        (1, 'Female'),
        (2, 'Male')
    )
    first_name = models.CharField(
        max_length=64,
        verbose_name='Name',
        blank=True,
        null=True
    )
    last_name = models.CharField(
        max_length=64,
        verbose_name='Surname',
        blank=True,
        null=True
    )
    email = models.EmailField(
        unique=True,
    )
    phone = models.CharField(
        max_length=16,
        unique=True,
        blank=True,
        null=True
    )
    avatar = models.ImageField(
        blank=True,
        null=True
    )
    gender = models.PositiveSmallIntegerField(
        choices=GENDER_CHOICES,
        null=True,
        blank=True
    )
    is_staff = models.BooleanField(
        default=False,
    )
    is_active = models.BooleanField(
        default=False,
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = CustomUserManager()

    class Meta:
        ordering = ('-created_at',)
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        return self.email
